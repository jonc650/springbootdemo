package tech.milgi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.milgi.model.Employee;

@RestController
public class EmployeeController
{
    @RequestMapping("/")
    public List<Employee> getEmployees()
    {
        List<Employee> employeesList = new ArrayList<Employee>();
        employeesList.add(new Employee(1,"Jon","Crocombe","jon@milgi.tech"));
        return employeesList;
    }
}
