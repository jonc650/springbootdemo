#!/usr/bin/python

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sys

testhost=sys.argv[1]

driver = webdriver.Remote(
   command_executor='http://selenium:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.FIREFOX)

# Note - port below is the in-container port as on the docker network
driver.get("http://"+testhost+":8090")
assert "Jon" in driver.page_source
driver.close()